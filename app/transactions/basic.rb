# auto_register: false
require "dry/transaction"

module Transactions
  class Basic
    include Dry::Transaction(container: PaymentApi::Container)
  end
end
