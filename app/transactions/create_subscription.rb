require_relative 'basic'

module Transactions
  class CreateSubscription < Transactions::Basic
    step :validate, with: 'operations.subscriptions.validate'
    step :register, with: 'operations.subscriptions.register'
    step :save,     with: 'operations.subscriptions.save'
  end
end
