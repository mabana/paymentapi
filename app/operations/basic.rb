# auto_register: false

require "dry/transaction/operation"

module Operations
  class Basic
    include Dry::Transaction::Operation
  end
end
