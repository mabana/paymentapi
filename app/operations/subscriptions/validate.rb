require 'dry/validation'

require_relative '../basic'

module Operations
  module Subscriptions
    class Validate < Operations::Basic
      Schema = Dry::Validation.Params do
        # TODO: Valid created card number
        required(:first_name).filled(:str?)
        required(:last_name).filled(:str?)
        required(:card_number).filled(:str?)
        required(:cv2).filled(format?: /\A\d{4}\z/)
        required(:expiration_month).filled(:int?, gt?: 0, lteq?: 12)
        required(:expiration_year).filled(:int?, gt?: 18, lteq?: 30)
      end

      private_constant :Schema

      def call(attrs)
        validation = Schema.(attrs)

        if validation.success?
          Success(validation.output)
        else
          Failure(validation.messages)
        end
      end
    end
  end
end
