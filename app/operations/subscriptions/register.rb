require_relative '../basic'

module Operations
  module Subscriptions
    class Register < Operations::Basic
      include PaymentApi::Import['services.api']

      def call(attrs)
        result = api.validate_subscription(attrs)

        if result
          if result['paid']
            attrs.merge!(billing_id: result['id']).delete(:cv2)
            Success(attrs)
          else
            Failure(message: result['failure_message'])
          end
        else
          Failure(message: 'timeout expired')
        end
      end
    end
  end
end
