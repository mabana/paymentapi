require_relative '../basic'

module Operations
  module Subscriptions
    class Save < Operations::Basic
      include PaymentApi::Import['db']

      def call(attrs)
        id = db[:subscriptions].insert(attrs)
        Success(
          attrs.merge(id: id)
        )
      end
    end
  end
end
