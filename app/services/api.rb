require 'faraday'
require 'json'
require 'net/http'

module Services
  class Api
    include PaymentApi::Import['config']

    NUMBER_OF_RETRIES = 5
    TIMEOUT           = 1
    URL               = 'http://127.0.0.1:4567'

    private_constant :NUMBER_OF_RETRIES
    private_constant :TIMEOUT

    def validate_subscription(attrs)
      response = get('/validate', attrs)

      if response.status == 200
        response.body
      else
        false
      end
    end

    private

    def get(uri, attrs)
      faraday.get(uri) do |request|
        request.options.timeout = TIMEOUT
      end
    end

    def faraday
      Faraday.new(url: URL) do |conn|
        conn.request(
          :retry,
          max:                  NUMBER_OF_RETRIES,
          interval:             0.05,
          interval_randomness:  0.5,
          backoff_factor:       2,
          exceptions:           ['Faraday::TimeoutError']
        )

        conn.basic_auth(*basic_auth_data)
        conn.adapter(Faraday.default_adapter)
      end
    end

    def basic_auth_data
      config['api'].values_at('key', 'secret')
    end
  end
end
