require 'dry/system/container'
require 'pathname'

module PaymentApi
  class Container < Dry::System::Container
    configure do |config|
      config.root = File.expand_path('../../..' ,__FILE__)
      config.auto_register = 'app'
    end

    load_paths!('app')
  end
end
