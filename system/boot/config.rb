PaymentApi::Container.boot(:config) do
  init do
    require 'yaml'
    config_path = PaymentApi::Container.root.join('config.yml')
    config      = YAML.load_file(config_path)

    register(:config, config)
  end
end
