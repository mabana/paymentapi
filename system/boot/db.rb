PaymentApi::Container.boot(:db) do |app|
  init do
    use :config
    require 'sequel'

    path = PaymentApi::Container.root.join(app['config']['database'])
    db   = Sequel.connect("sqlite://#{path}")

    register(:db, db)
  end
end
