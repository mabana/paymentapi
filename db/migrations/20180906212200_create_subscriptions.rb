Sequel.migration do
  change do
    create_table(:subscriptions) do
      primary_key :id

      String :first_name, null: false
      String :last_name, null: false
      Integer :billing_id, null: false
      Integer :card_number, null: false
      Integer :expiration_month, null: false
      Integer :expiration_year, null: false
    end
  end
end
