require 'sinatra'
require 'json'
require_relative 'system/boot'

class App < Sinatra::Base
  get '/' do
    'Hello World'
  end

  post '/subscriptions' do
    content_type :json

    PaymentApi::Container['transactions.create_subscription'].(params) do |m|
      m.success do |record|
        record.to_json
      end

      m.failure :register do |result|
        status(402)
        result.merge(status: 402).to_json
      end

      m.failure :validate do |messages|
        messages.to_json
      end
    end
  end
end
